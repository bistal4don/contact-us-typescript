This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Table of Contents

- [Set Up](#set-up)
- [Folder Structure](#folder-structure)
- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)
  - [npm test](#npm-test)

## Set Up
 How to setup locally,
```
1.  copy and paste "git clone https://gitlab.com/bistal4don/contact-us-typescript.git" to your prefered file path. 
2.  open the project in visual studio or your prefered IDE. 

###  `npm install`

    if using visual studio code, open terminal and type "npm install". if not, you can open the project
    folder 'using command bash in windows' or 'using terminal in mac' and then type the same command
    
3.  Then, type "npm start" to run
4.  Then, type "npm test" to test
```

## Folder Structure

 The project should look like this:

```
contact-us-typescript/
  README.md
  __mocks__
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    __snapshots__/
        App.test.tsx.snap
    actions/
        form-action.test.tsx
        form-action.tsx
    components/
        Error/
            ErrorMessage.css
            ErrorMessage.test.tsx
            ErrorMessage.tsx
        Form/
            Form.css
            Form.test.tsx
            Form.tsx
    images/
        sent.png
    mocks/
        contact-mock.ts
        localstorage-mock.ts
        store-setup.mock.ts
    models/
        functions/
            form-functions.test.tsx
            form-functions.tsx
        interfaces/
            form-interface.tsx
    reducers/
        form-reducer.test.tsx
        form-reducer.tsx
    App.css
    App.tsx
    App.test.tsx
    index.css
    index.tsx
    logo.svg
tests/
    server.js
    setupTests.js
```

## Available Scripts

Firt of all , type `npm install npm@latest -g` if you have not tyoed it already.

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner using Jest.

