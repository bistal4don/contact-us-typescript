import {FormControl, Button} from 'react-bootstrap'

export interface IStoreState {
    type?: string
    details?: IContactDetails
    
    OnChange?: (e: React.FormEvent<FormControl>) => void ;
    OnSubmit?: (e: React.FormEvent<Button>) => void ;
    canSubmit?: boolean
    errors?: any
    
}

export interface IContactDetails {
    age?: number;
    message?: string
    email?: string 
    firstName?: string
    lastName?: string
} 

export interface IErrors {
    message: string;
}

export interface IPassedProps {
    contactDetails: IStoreState
    onSubmitContactForm?: any
    onUpdateAndStore?: any
}


export interface IMyComponentState { 
    email :  string ,
    message: string,
    firstName: string,
    lastName: string, 
    hasSubmitted: boolean
  }