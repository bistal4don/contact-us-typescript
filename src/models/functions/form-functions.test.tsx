import { validate, hasWhiteSpace } from './form-functions';
import { contactMessage } from '../../mocks/contact-mock'

const {email, message, firstName, lastName} = contactMessage();

test('test validate method should return false', () => {
  expect(validate(email||'', message||'', firstName||'', lastName||'')).toEqual({email: false, message: false, firstName: false, lastName: false});
});
test('test has wwhite space and should return true', () => {
  expect(hasWhiteSpace('White Space')).toBe(true);
});