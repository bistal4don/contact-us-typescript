
export function validate(email: string, message: string, firstName: string, lastName: string) {

  return {
    email: email.length === 0 || (hasWhiteSpace(email) && email.trim().length === 0),
    message: message.length === 0 || (hasWhiteSpace(message) && message.trim().length === 0),
    firstName: firstName.length === 0 || (hasWhiteSpace(firstName) && firstName.trim().length === 0),
    lastName: lastName.length === 0 || (hasWhiteSpace(lastName) && lastName.trim().length === 0),
  };
}

export function hasWhiteSpace(value: string) {
  return /\s/g.test(value);
}
