import  { FORM_RESPONSE_SUCCESS, FORM_RESPONSE_ERROR, FORM_RESET_SUCCESS } from '../actions/form-action'
import { IStoreState } from 'src/models/interfaces/form-interface';
import { storeMock } from '../mocks/contact-mock'

export default function formReducer(state:IStoreState = storeMock(), action: any) {
    switch (action.type) {
        case FORM_RESPONSE_SUCCESS:
            return action;
        case FORM_RESPONSE_ERROR:
            return action;
        case FORM_RESET_SUCCESS:
            return action;
        default:
            return state;
    }
}