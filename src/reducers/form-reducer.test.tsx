import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import formReducer from './form-reducer'
import { storeMock, sucStoreMock } from '../mocks/contact-mock'
import  { FORM_RESPONSE_SUCCESS } from '../actions/form-action'
import 'jest-localstorage-mock'

configure({
    adapter: new Adapter(),
  })


  const state = storeMock();
  const action = sucStoreMock();

describe('Form-reducers', () => {
  
    test('should retuen sucessfull action', () => {
        action.type = FORM_RESPONSE_SUCCESS;
        const reducerWrapper = formReducer(state, action);

        expect(reducerWrapper).toBe(action);
    });

});