import * as React from 'react';
import { StoreBuilder } from 'redux-ts'
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { combineReducers} from 'redux';
import formReducer from './reducers/form-reducer';
import { IStoreState } from './models/interfaces/form-interface'
import  thunk  from 'redux-thunk';
// import { storeMock } from './mocks/contact-mock'



const allReducers = combineReducers({
    form: formReducer
})

const store = new StoreBuilder<IStoreState>()
                        .withReducer("reducer", allReducers)
                        // .withInitialState(storeMock())
                        .withDevTools() // enable chrome devtools
                        .withMiddleware(thunk)
                        .build();

ReactDOM.render(
  <Provider store={store}>  <App /></Provider>
  ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
