import { IStoreState } from '../../models/interfaces/form-interface'
import { Alert } from 'react-bootstrap'
import * as React from 'react';
import './ErrorMessage.css';


const ErrorMessage  = (props: IStoreState) => {
      return (
          <div className="error">
               <Alert bsStyle="danger" >
                 <h4>{ props.details }</h4>
                </Alert>
          </div>
      );
};

export default ErrorMessage;