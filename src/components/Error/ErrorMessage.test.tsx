import * as React from 'react';
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import { errStoreMock } from '../../mocks/contact-mock'
import  ErrorMessage  from './ErrorMessage'

configure({
    adapter: new Adapter(),
  })


describe('ErrorMessage ', () => {
    it('renders error mesage', () => {
    
        const { errors } = errStoreMock();

      const wrapper = shallow(<ErrorMessage details={errors}/>)
      expect(wrapper.find('.error')).toBeDefined();
      expect(wrapper.find('h4').html()).toMatch('Error, Please try again');
    })
});