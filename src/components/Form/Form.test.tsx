import * as React from 'react';
import Adapter from 'enzyme-adapter-react-16'
import { configure, shallow } from 'enzyme'
import  Form  from './Form'
import {errMock} from '../../mocks/contact-mock'

configure({
    adapter: new Adapter(),
  })

describe('Form ', () => {

    it('renders error mesage', () => {

      const wrapper = shallow(<Form errors={errMock}/>)

      expect( wrapper.find('.text').get(0).props.children).toMatch('First Name:');
      expect( wrapper.find('.text').get(1).props.children).toMatch('Last Name:');
      expect( wrapper.find('.text').get(2).props.children).toMatch('Email Address:');
      expect( wrapper.find('.text').get(3).props.children).toMatch('Message:');
  
    })
});