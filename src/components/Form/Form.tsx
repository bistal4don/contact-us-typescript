import { IStoreState } from 'src/models/interfaces/form-interface';
import * as React from 'react';
import { FormGroup, FormControl, Button, ControlLabel} from 'react-bootstrap'
import './Form.css';

const Form  = (props: IStoreState) => {

    return (
    <div className="App">
        <div className="contactUs">Contact Us</div>
        <form className="form">
            <FormGroup validationState={props.errors.firstName ? 'error' : 'success'} >
                <ControlLabel className="text">First Name: </ControlLabel>
                    <FormControl className='input' id="firstName" name="firstName" placeholder="First Name" onChange={props.OnChange} required={true} />
                <FormControl.Feedback />
            </FormGroup >

            <FormGroup validationState={props.errors.lastName ? 'error' : 'success'} >
                <ControlLabel className="text">Last Name: </ControlLabel>
                    <FormControl className='input' name="lastName" placeholder="Last Name" onChange={props.OnChange} required={true}/>
                <FormControl.Feedback />
            </FormGroup> 

            <FormGroup validationState={props.errors.email ? 'error' : 'success'} >
                <ControlLabel className="text">Email Address: </ControlLabel> 
                    <FormControl type="email" className='input' name="email" placeholder="Email" onChange={props.OnChange} required={true}/>
                <FormControl.Feedback />
            </FormGroup> 

            <FormGroup validationState={props.errors.message ? 'error' : 'success'} >
                <ControlLabel className="text">Message: </ControlLabel> 
                    <FormControl componentClass="textarea" className='input' name="message" placeholder="Message" onChange={props.OnChange} required={true}/>
                <FormControl.Feedback />
            </FormGroup>  

            <Button className="button" bsStyle="primary" type="button" onClick={props.OnSubmit} disabled={props.canSubmit}>Send</Button>
        
        </form>
    </div>
    );
}

export default Form;