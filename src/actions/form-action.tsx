import { IContactDetails } from '../models/interfaces/form-interface'

export const FORM_RESPONSE_ERROR = 'form:showError';
export const FORM_RESPONSE_SUCCESS = 'form:submitted';
export const FORM_RESET_SUCCESS = 'form:cleared';

const WRONG_EMAIL_FORMAT =' You have enter the wrong email format, please try again'

export function addToStorage(state: IContactDetails) {
    return (dispatch: any) => {
        if(localStorage.getItem('contactsList')==null) {
            const contactsList: any = [];
            contactsList.push(state)
            localStorage.setItem('contactsList', JSON.stringify(contactsList))
        }else {
            const contactsList = JSON.parse(localStorage.getItem('contactsList') || '{}');
            contactsList.push(state)
            localStorage.setItem('contactsList', JSON.stringify(contactsList))
        }
    }
}

export function showError(info: string) {
    return {
        type: FORM_RESPONSE_ERROR,
        errors: info
    }
}

export function  submitContactForm(newContact: IContactDetails) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

     if(!re.test(newContact.email||'')) {
        return showError(WRONG_EMAIL_FORMAT);
    } 

    return {
        type: FORM_RESPONSE_SUCCESS,
        details: newContact
    }
    
}

export function  resetForm(newContact: null) {
    return {
        type: FORM_RESET_SUCCESS,
        details: newContact
    }
}



