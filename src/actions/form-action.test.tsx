import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import * as actions from './form-action'
import { contactMessage } from '../mocks/contact-mock'
import LocalStorageMock from '../mocks/localstorage-mock'
import 'jest-localstorage-mock'

configure({
  adapter: new Adapter(),
})

const localStorage = new LocalStorageMock()

beforeEach(() => {
  // values stored in tests will also be available in other tests unless you run
  localStorage.clear();

  // you could also reset all mocks, but this could impact your other mock
  jest.resetAllMocks();
});

test('local storage should return value when via key', () => {
  const ex =  {
    KEY : 'foo',
    VALUE : 'bar',
  }

  localStorage.setItem(ex.KEY, ex.VALUE)

  expect(localStorage.getItem('foo')).toBe('bar');
});

describe('Form-actions', () => {
    it('should create an action to show error', () => {
      const errForm = 'Error Test'
      const expectedAction = {
        type: actions.FORM_RESPONSE_ERROR,
        errors: errForm
      }
      expect(actions.showError(errForm)).toEqual(expectedAction)
    })

    it('should create an action to submit contact us form', () => {
      const form = contactMessage();
      const expectedAction = {
        type: actions.FORM_RESPONSE_SUCCESS,
        details: form
      }
      expect(actions.submitContactForm(form)).toEqual(expectedAction)
    })

    it('should create an action to reset form', () => {
      const resetForm = null
      const expectedAction = {
        type: actions.FORM_RESET_SUCCESS,
        details: resetForm
      }
      expect(actions.resetForm(resetForm)).toEqual(expectedAction)
    })
  })