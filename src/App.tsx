import * as React from 'react';
import './App.css';
import Form from './components/Form/Form'
import ErrorMessage from './components/Error/ErrorMessage'
import { connect } from 'react-redux';
import { submitContactForm, addToStorage, FORM_RESPONSE_SUCCESS, FORM_RESPONSE_ERROR } from "./actions/form-action";
import { IPassedProps, IMyComponentState } from './models/interfaces/form-interface'
import { validate } from './models/functions/form-functions'
import { FormControl, Button } from 'react-bootstrap'
import sentLogo from './images/sent.png'

class App extends React.Component<IPassedProps, IMyComponentState> {

  constructor(props:any) {
    super(props);
    this.state = {
      email: '',
      message: '',
      firstName: '',
      lastName: '', 
      hasSubmitted: false,
    };
  }

  public componentDidUpdate(prevSate: any) {
    if( prevSate !== this.props) {
        const { contactDetails } = this.props;
        if(contactDetails.type === FORM_RESPONSE_SUCCESS) {
            this.props.onUpdateAndStore(contactDetails.details);
            this.setState({
              hasSubmitted: true,
            })
        }
    }
  }

  public change = (e: React.FormEvent<FormControl>) => {
    const {name, value}: any = e.target;
    this.setState({
        [name]: value
    } as any);
  };

  public submit = (e: React.FormEvent<Button>) => {
    e.preventDefault();
    this.props.onSubmitContactForm(this.state);
  };

  public back = () => {
    this.setState({
      hasSubmitted: false,
      email: '',
      firstName: '',
      lastName: '',
      message: '',
    })
    this.props.contactDetails.details = {}
  }

  public canBeSubmitted() {
    const errors = validate(this.state.email, this.state.message, this.state.firstName, this.state.lastName);
    const isDisabled = Object.keys(errors).some(x => errors[x]);
    return isDisabled;
  }

  public render() {

    const showError = (this.props.contactDetails.type === FORM_RESPONSE_ERROR)
    const hasSubmitted = this.state.hasSubmitted;

    if(!hasSubmitted) {
      return (
        <div id="form-section" className="App">
        <span id="errorId">
        { showError ? <ErrorMessage details={this.props.contactDetails.errors}/> : null }
        </span>
        
          <Form OnChange={this.change} OnSubmit={this.submit} 
          canSubmit={this.canBeSubmitted()} errors={validate(this.state.email, this.state.message, this.state.firstName, this.state.lastName)}
          />
        </div>
      );
    } else {
       return( 
          <div id="submitted" className="App"> 
             <img className="sentLogo" src={sentLogo} alt="sent"/>
             <span className="sentInfo"> We have received your query and we would get back to you as soon as possible. Thank you very much.</span>
             <Button className="sentBack" bsStyle="success" onClick={this.back}> Submit Again</Button>
          </div>
      );
    }
  }
}

const mapStateToProps = (state: any) => ({
  contactDetails: state.reducer.form
});

const mapActionsToProps = {
  onSubmitContactForm: submitContactForm, 
  onUpdateAndStore: addToStorage
};

export default connect(mapStateToProps, mapActionsToProps) (App);
