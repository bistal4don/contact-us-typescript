
class LocalStorageMock {
    private store = {}
  
    public clear() {
      this.store = {}
    }
  
    public getItem(key:string) {
      return this.store[key] || null
    }
  
    public setItem(key: string, value: string) {
      this.store[key] = value
    }
  
    public removeItem(key: string) {
      delete this.store[key]
    }
  }

  export default LocalStorageMock;