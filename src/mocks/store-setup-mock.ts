import { combineReducers } from 'redux';
import formReducer from '../reducers/form-reducer';
import thunk from 'redux-thunk';
import { StoreBuilder } from 'redux-ts'
import { IStoreState } from '../models/interfaces/form-interface'

const allReducers = combineReducers({
  
    form: formReducer
  
})

export const store = new StoreBuilder<IStoreState>()
                        .withReducer("reducer", allReducers)
                        .withMiddleware(thunk)
                        .build();
