import { IStoreState, IContactDetails } from '../models/interfaces/form-interface'
import { FORM_RESPONSE_ERROR, FORM_RESPONSE_SUCCESS} from '../actions/form-action'

export function errMock() { return {email: true, message: true, firstName: true, lastName: true}}

export const storeMock =(): IStoreState => ({
    type: '', 
    details: contactMessage(), 
    
    canSubmit:false,
    errors: ''
   });
   
export const sucStoreMock =(): IStoreState => ({
    type: FORM_RESPONSE_SUCCESS, 
    details: contactMessage(), 

    canSubmit:false,
    errors: ''
});


export const contactMessage = (): IContactDetails => ({
     age: 21,
     message: 'This is a test',
     email: 'bisi.ade@test.com',
     firstName: 'Bisi',
     lastName: 'Aden'
   });

export const emptyContactMessage = (): IContactDetails => ({
    age: 21,
    message: '',
    email: '',
    firstName: '',
    lastName: ''
});

export const errStoreMock =(): IStoreState => ({
    type: FORM_RESPONSE_ERROR, 
    details: errorContactMessage(), 

    canSubmit:false,
    errors: 'Error, Please try again'
});


export const errorContactMessage = (): IContactDetails => ({
    message: '',
});