import * as React from 'react';
import { configure, mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import toJson from 'enzyme-to-json'
import { sucStoreMock, errMock } from './mocks/contact-mock'
import { store } from './mocks/store-setup-mock'
import App from './App';
import { Provider } from 'react-redux';

configure({
  adapter: new Adapter(),
})



function setup() {
  const props = {
    contactDetails: sucStoreMock()
  }

  const enzymeWrapper = mount(<Provider store={store}> <App /> </Provider>)

  return {
    props,
    enzymeWrapper
  }
}


describe('App Component', () => {

  test('should render', () => {
    const { enzymeWrapper } = setup()
    const tree = toJson(enzymeWrapper);
    expect(tree).toMatchSnapshot();
  });


  test('should render initial state of self and subcomponents', () => {
    const { enzymeWrapper } = setup()

    
    expect(enzymeWrapper.find('#form-section').hasClass('App')).toBe(true)
    

    const error = errMock();
    expect(enzymeWrapper.find('#errorId').text()).toBe("")

    const formProps : any = enzymeWrapper.find('Form').props() 
    expect(formProps.canSubmit).toBe(true)
    expect(formProps.errors).toEqual(error)
  })
})

